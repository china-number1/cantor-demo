package com.matrix.cantordemo.userbusiness;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@Slf4j
public class UserBusinessApplication {

    public static void main(String[] args) {
        SpringApplication.run(UserBusinessApplication.class, args);
    }

}
