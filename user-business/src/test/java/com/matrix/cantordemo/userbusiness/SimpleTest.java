package com.matrix.cantordemo.userbusiness;

import cn.hutool.core.lang.Console;
import com.cantor.cantorspringbootstarter.anno.CantorReference;
import com.cantor.consumer.future.FuturesKeeper;
import com.cantor.consumer.handler.PongHandler;
import com.cantor.core.center.RegistrationCenter;
import com.cantor.core.center.impl.ZooKeeperRegistrationCenter;
import com.cantor.core.handler.CantorMessageCodec;
import com.cantor.core.handler.StickAndHalfPackageDecoder;
import com.cantor.core.message.CantorRequestMessage;
import com.cantor.core.message.CantorResponseMessage;
import com.cantor.core.pool.CantorExecutorPool;
import com.matrix.cantordemo.userbusiness.serviceimpl.UserServiceImpl;
import io.netty.bootstrap.Bootstrap;
import io.netty.channel.*;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioSocketChannel;
import io.netty.handler.timeout.IdleStateHandler;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Field;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
import java.util.function.IntConsumer;
import java.util.stream.IntStream;

@Slf4j
public class SimpleTest {

    private RegistrationCenter center;

    @BeforeEach
    public void init() {
        center = new ZooKeeperRegistrationCenter().setNamespace("cantor");
        center.run();
    }

    @AfterEach
    public void destroy() {
        center.close();
    }

    // 测试Field获取Ann
    @Test
    public void testGetAnn() {
        for (Field f : UserServiceImpl.class.getDeclaredFields()) {
            if (f.isAnnotationPresent(CantorReference.class)) {
                log.info("----------------------------------------------");
                log.info("所有注解数量: {}", f.getDeclaredAnnotations().length);
                log.info("得到注解了: {}", f.getDeclaredAnnotation(CantorReference.class));
            }
        }
    }

    // 测试CPU核数
    @Test
    public void testProcessors() {
        System.out.println(Runtime.getRuntime().availableProcessors());
    }

    // 测试IntStream 00
    // 测试结果: 只有小于第二数字才会执行,0,0不会执行
    @Test
    public void testIntStream00() {
        IntStream.range(0, 1).forEach(new IntConsumer() {
            @Override
            public void accept(int value) {
                System.out.println("我被调用了" + value);
            }
        });
    }

    // 测试手动向Provider端发送Request请求
    public Channel runNettyClient() throws InterruptedException {
        // 发送CantorRequestMessage请求
        EventLoopGroup group = new NioEventLoopGroup(1);
        Bootstrap bootstrap = new Bootstrap().group(group)
                .channel(NioSocketChannel.class)
                .option(ChannelOption.CONNECT_TIMEOUT_MILLIS, 5000)
                .option(ChannelOption.RCVBUF_ALLOCATOR, new FixedRecvByteBufAllocator(65535))
                .option(ChannelOption.TCP_NODELAY, true) // 直接发包
                .handler(new ChannelInitializer<NioSocketChannel>() {
                    @Override
                    protected void initChannel(NioSocketChannel ch) throws Exception {
                        ChannelPipeline pipeline = ch.pipeline();
                        // Duplex
                        // pipeline.addLast(new LoggingHandler()); // 日志
                        pipeline.addLast(new IdleStateHandler(10L, 0, 0, TimeUnit.SECONDS)); // 读空闲处理器
                        pipeline.addLast(new CantorMessageCodec()); // 编解码器
                        // Out

                        // In
                        pipeline.addLast(new StickAndHalfPackageDecoder()); // 半包黏包处理器
                        pipeline.addLast(new PongHandler()); // 心跳包处理器
                        pipeline.addLast(new SimpleChannelInboundHandler<CantorResponseMessage>() {
                            @Override
                            protected void channelRead0(ChannelHandlerContext ctx, CantorResponseMessage res) {
                                log.info("读到回应" + res.getSequenceId());
                                FuturesKeeper.checkout(res.getSequenceId()).complete(res.getReturnValue());
                            }
                        });
                    }
                });
        return bootstrap.connect("127.0.0.1", 8101)
                .sync()
                .channel();
    }

    // 测试发送请求
    @Test
    public void testNettyClient() throws InterruptedException {
        Channel ch = runNettyClient();
        final int nThreads = 300;
        final CountDownLatch latch = new CountDownLatch(nThreads);
        for (int i = 0; i < nThreads; i++) {
            CompletableFuture.runAsync(() -> {
                long sequenceId = 0;
                try {
                    sequenceId = center.getSequenceId();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                long start = System.currentTimeMillis();
                CompletableFuture future = FuturesKeeper.record(sequenceId, new CompletableFuture());
                CantorRequestMessage requestMessage = CantorRequestMessage.builder()
                        .interfaceName("com.matrix.cantordemo.api.service.ProductRpcService")
                        .serviceVersion("")
                        .methodName("singleCall")
                        .parameterTypes(new Class[]{String.class})
                        .parameterValue(new Object[]{"Message-" + sequenceId})
                        .returnType(String.class)
                        .build();
                requestMessage.setSequenceId(sequenceId);
                long finalSequenceId = sequenceId;
                ch.writeAndFlush(requestMessage).addListener((ChannelFutureListener) channelFuture -> Console.log("消息{}成功发送", finalSequenceId));
                future.join(); // blocked
                Console.error("{} 耗时: {}", sequenceId, System.currentTimeMillis() - start);
                latch.countDown();
            }, CantorExecutorPool.pool());
        }

        latch.await();
        System.out.println(nThreads + "次请求全部完成");

    }


}
