package com.matrix.cantordemo.productbusiness;

import com.baomidou.mybatisplus.generator.FastAutoGenerator;
import com.baomidou.mybatisplus.generator.config.OutputFile;
import com.baomidou.mybatisplus.generator.engine.FreemarkerTemplateEngine;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Collections;

@SpringBootTest
class ProductBusinessApplicationTests {

    @Test
    void contextLoads() {
        FastAutoGenerator.create("jdbc:mysql://localhost:3306/bigdata?serverTimezone=Asia/Shanghai&useUnicode=true&characterEncoding=UTF-8", "root", "root")
                .globalConfig(builder -> {
                    builder.author("matrix") // 设置作者
                            .fileOverride() // 覆盖已生成文件
                            .outputDir("D:\\projects\\java\\cantor-demo\\product-business\\src\\main\\java\\com\\matrix\\cantordemo\\productbusiness"); // 指定输出目录
                })
                .packageConfig(builder -> {
                    builder.parent("com.matrix.cantordemo") // 设置父包名
                            .moduleName("productbusiness") // 设置父包模块名
                            .pathInfo(Collections.singletonMap(OutputFile.mapperXml, "D:\\projects\\java\\cantor-demo\\product-business\\src\\main\\resources\\mapper")); // 设置mapperXml生成路径
                })
                .strategyConfig(builder -> {
                    builder.addInclude("t_sku") // 设置需要生成的表名
                            .addTablePrefix("t_"); // 设置过滤表前缀
                })
                .templateEngine(new FreemarkerTemplateEngine()) // 使用Freemarker引擎模板，默认的是Velocity引擎模板
                .execute();


    }

}
