package com.matrix.cantordemo.productbusiness.mapper;

import com.matrix.cantordemo.api.entity.Sku;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;

/**
 * <p>
 * 商品表 Mapper 接口
 * </p>
 *
 * @author matrix
 * @since 2022-02-21
 */
@Repository
public interface SkuMapper extends BaseMapper<Sku> {

}
