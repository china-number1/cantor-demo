package com.matrix.cantordemo.productbusiness.controller;

import com.matrix.cantordemo.api.entity.Sku;
import com.matrix.cantordemo.productbusiness.service.ISkuService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * <p>
 * 商品表 前端控制器
 * </p>
 *
 * @author matrix
 * @since 2022-02-21
 */
@RestController
@RequestMapping("/product")
public class SkuController {

    @Autowired
    ISkuService skuService;

    @GetMapping("")
    List<Sku> get(){
        return skuService.userProducts();
    }

}
