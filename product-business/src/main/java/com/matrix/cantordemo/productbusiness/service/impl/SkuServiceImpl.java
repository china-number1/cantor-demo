package com.matrix.cantordemo.productbusiness.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.github.pagehelper.PageHelper;
import com.matrix.cantordemo.api.entity.Sku;
import com.matrix.cantordemo.productbusiness.mapper.SkuMapper;
import com.matrix.cantordemo.productbusiness.service.ISkuService;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 商品表 服务实现类
 * </p>
 *
 * @author matrix
 * @since 2022-02-21
 */
@Service
public class SkuServiceImpl extends ServiceImpl<SkuMapper, Sku> implements ISkuService {

    @Override
    public List<Sku> userProducts() {
        PageHelper.startPage(1,10);
        return list();
    }

}
