package com.matrix.cantordemo.productbusiness.service.impl;

import com.cantor.cantorspringbootstarter.anno.CantorService;
import com.matrix.cantordemo.api.entity.Sku;
import com.matrix.cantordemo.api.service.ProductRpcService;
import com.matrix.cantordemo.productbusiness.service.ISkuService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@CantorService
@Slf4j
public class ProductRpcServiceImpl implements ProductRpcService {

    @Autowired
    ISkuService skuService;

    @Override
    public String singleCall(String msg) {
        return "回应"+msg;
    }

    @Override
    public List<Sku> userProducts() {
        long start = System.currentTimeMillis();
        List<Sku> skus = skuService.userProducts();
        log.info("查数据库耗时: {}",System.currentTimeMillis() - start);
        return skus;
    }

}
