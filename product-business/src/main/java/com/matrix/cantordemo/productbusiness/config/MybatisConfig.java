package com.matrix.cantordemo.productbusiness.config;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@MapperScan("com.matrix.cantordemo.productbusiness.mapper")
public class MybatisConfig {



}
