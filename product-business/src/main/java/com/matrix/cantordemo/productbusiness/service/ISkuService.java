package com.matrix.cantordemo.productbusiness.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.matrix.cantordemo.api.entity.Sku;

import java.util.List;

/**
 * <p>
 * 商品表 服务类
 * </p>
 *
 * @author matrix
 * @since 2022-02-21
 */
public interface ISkuService extends IService<Sku> {

    List<Sku> userProducts();

}
