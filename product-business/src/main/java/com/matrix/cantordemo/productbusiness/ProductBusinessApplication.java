package com.matrix.cantordemo.productbusiness;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProductBusinessApplication {

    public static void main(String[] args) {
        SpringApplication.run(ProductBusinessApplication.class, args);
    }

}
