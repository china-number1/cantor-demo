package com.matrix.cantordemo.common.entity;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class GlobalResponse<T> {
    private int code;
    private T data;
    private String message;
    private long time;

    public static <T> GlobalResponse ok(T data){
        return new GlobalResponse<T>().setCode(0).setData(data);
    }

    public static GlobalResponse fail(String msg){
        return new GlobalResponse().setCode(1).setMessage(msg);
    }

    public static GlobalResponse fail(){
        return new GlobalResponse().setCode(1).setMessage("Unknown Error Occurred");
    }

}
