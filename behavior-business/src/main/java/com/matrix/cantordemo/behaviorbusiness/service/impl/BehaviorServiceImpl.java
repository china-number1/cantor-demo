package com.matrix.cantordemo.behaviorbusiness.service.impl;

import com.cantor.cantorspringbootstarter.anno.CantorService;
import com.matrix.cantordemo.api.service.BehaviorService;
import org.springframework.stereotype.Service;

@Service
// @CantorService(mock = "force:return null")
@CantorService
public class BehaviorServiceImpl implements BehaviorService {

}
