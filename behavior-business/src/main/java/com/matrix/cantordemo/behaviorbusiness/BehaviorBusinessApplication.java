package com.matrix.cantordemo.behaviorbusiness;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BehaviorBusinessApplication {

    public static void main(String[] args) {
        SpringApplication.run(BehaviorBusinessApplication.class, args);
    }

}
