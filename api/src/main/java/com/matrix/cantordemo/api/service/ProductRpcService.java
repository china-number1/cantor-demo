package com.matrix.cantordemo.api.service;

import com.matrix.cantordemo.api.entity.Sku;

import java.util.List;

public interface ProductRpcService {

    String singleCall(String msg);

    List<Sku> userProducts();

}
