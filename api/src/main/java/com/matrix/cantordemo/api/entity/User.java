package com.matrix.cantordemo.api.entity;

import lombok.Data;
import lombok.experimental.Accessors;

import java.util.*;

@Data
@Accessors(chain = true)
public class User {

    public static final Map<Long, User> userDb = new HashMap<Long, User>() {{
        put(1L, new User().setId(1L).setUsername("张三").setHobbies(Arrays.asList("抽烟", "喝酒", "烫头")));
        put(2L, new User().setId(2L).setUsername("李四").setHobbies(Arrays.asList("打豆豆")));
        put(3L, new User().setId(3L).setUsername("王五").setHobbies(Arrays.asList("吃饭", "睡觉", "打游戏")));
        put(4L, new User().setId(4L).setUsername("赵六").setHobbies(Arrays.asList()));
        put(5L, new User().setId(5L).setUsername("钱七").setHobbies(Arrays.asList("跑步")));
    }};

    private Long id;
    private String username;
    private List<String> hobbies;

}
