package com.matrix.cantordemo.api.entity;

import lombok.Data;
import lombok.experimental.Accessors;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

@Data
@Accessors(chain = true)
public class Behavior {

    public static Long sequence = 0L;

    public static final Map<Long, Behavior> behaviorDb = new HashMap<Long, Behavior>() {{
        put(1L, new Behavior().setId(1L).setTime(LocalDateTime.of(2021, 12, 12, 14, 33)).setAction("购买"));
    }};

    private Long id;
    private String action;
    private LocalDateTime time;
    private String message;

}
